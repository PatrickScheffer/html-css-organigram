# HTML and CSS organigram

Create organigrams with only HTML and CSS. No canvas or Javascript required. Based on an article written by Robert Wenzel: https://dzone.com/articles/css-flex-based-orgchart-with-zk.

## Example

```html
<ul class="orgchart">
  <li class="root">
    <div class="nodecontent">Root item</div>
    <ul>
      <li>
        <div class="nodecontent">Child item 1</div>
      </li>
      <li>
        <div class="nodecontent">Child item 2</div>
      </li>
    </ul>
  </li>
</ul>
```

## Files

* orgchart-base.css contains the CSS for creating the organigram.
* orgchart-layout.css contains the styling for the organigram and provides examples how to customize it.

## Documentation

### HTML
```html
<ul class="orgchart">
  <!-- Always start with a root. -->
  <li class="root">
    <!--
    This div contains the actual styled content of the item.
    It can also be a link: <a href="#" class="nodecontent">Text</a>.
    -->
    <div class="nodecontent">President</div>

    <!--
    Nodeassists can have max 2 children. These are mostly used for staff or
    support functions.
    -->
    <ul class="nodeassists">
      <!--
      Add the "leaf" class if the item has no children. Otherwise a line will
      be attached below the item.
      -->
      <li class="leaf">
        <div class="nodecontent">Secretary 1</div>
      </li>
      <li>
        <div class="nodecontent">Secretary 2</div>

        <!-- A nodeassist can have children. -->
        <ul>
          <li class="leaf">
            <div class="nodecontent">Art / Copy</div>
          </li>
          <li class="leaf">
            <div class="nodecontent">Production</div>
          </li>
        </ul>
      </li>
    </ul>

    <!-- A new nodeassists ul is started if you want to add more assists. -->
    <ul class="nodeassists">
      <li class="leaf">
        <div class="nodecontent">Secretary 3</div>
      </li>
      <!-- Add a dummy item if there is only 1 nodeassist. -->
      <li class="dummy"></li>
    </ul>

    <!-- Children can have children. -->
    <ul>
      <li>
        <div class="nodecontent">Vice President<br />Account Services</div>
        <ul>
          <li class="leaf">
            <div class="nodecontent">Account Supervisor</div>
          </li>
          <li class="leaf">
            <div class="nodecontent">Account Supervisor</div>
          </li>
        </ul>
      </li>
      <li>
        <div class="nodecontent">Vice President<br />Management Services</div>

        <!--
        Add the "vertical" class if you want the children to be displayed
        below each other.
        -->
        <ul class="vertical">
          <li class="leaf">
            <div class="nodecontent">Accounting</div>
          </li>
          <li class="leaf">
            <div class="nodecontent">Purchasing</div>
          </li>
          <li class="leaf">
            <div class="nodecontent">Personnel</div>
          </li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
```

### CSS

| Classname | Element | Description |
| --------- | ------- | ----------- |
| orgchart | ul | Without this, no organigram. |
| root | li | Every organigram starts with the boss. |
| nodecontent | div or a | Styles an organigram item. |
| nodeassists | ul | For children who act as staff or support functions. |
| dummy | li | Nodeassists always come in 2, so if there is only 1 the dummy should be used to get the number to 2. |
| leaf | li | A child without any further children. |
| vertical | ul | Displays the children above each other. |
